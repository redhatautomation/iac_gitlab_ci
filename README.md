# Infrastructure as Code for GitLab CI Ansible Roles

This repo hosts the roles for the `redhatautomation.iac_gitlab_ci` Ansible Collection. 

The collection includes the following roles:
  * `configure`
  * `manage_ec2_instances`
  * `slack`

For more information see the individual role's documentation.

## Python & Libraries
#### Python Version Compatibility
  * this collection is compatible with Python 2.6+

#### Python libraries
The following python libraries are required to use this collection:
  * `boto`
  * `boto3`
  * `botocore`

## Installation
Before using the this collection, you need to install it with the Ansible Galaxy CLI:

```console
    ansible-galaxy collection install redhatautomation.iac_gitlab_ci
```

You can also include it in a `requirements.yml` file and install it via `ansible-galaxy collection install -r requirements.yml`, using the format:

```yaml
---
collections:
  - name: redhatautomation.iac_gitlab_ci
    version: 1.0.9
```

#### Python Dependencies
In order to use this collection, the host which runs it (usually Tower) will need to have the required python libraries installed. The following command will install them:
```console
  pip install boto boto3 botocore
```

## Setup
In order for this collection to function it requires the loose integration of [Red Hat Ansible Automation Platform Controller](https://www.ansible.com/products/automation-platform) and an instance of [GitLab](https://about.gitlab.com/install/). 

#### Ansible Automation Platform Controller/Tower
Ansible Tower must be configured to use the same git repository as a [project](https://docs.ansible.com/ansible-tower/latest/html/userguide/projects.html). Each stage in GitLab CI will correspond to a [job template](https://docs.ansible.com/ansible-tower/latest/html/userguide/job_templates.html) and thus each job template should be created from the same configured project.

Playbooks used for this project resemble the following:

```yaml
---
- name: Configure Apache and Firewall
  hosts: rhautomation
  vars_files:
    - ec2_vars.yaml
  roles:
    - redhatautomation.iac_gitlab_ci.configure
```

```yaml
---
- name: Create lab instances in AWS
  hosts: localhost
  vars_files:
    - ec2_vars.yaml
  roles:
    - redhatautomation.iac_gitlab_ci.manage_ec2_instances
```


```yaml
---
- name: AWS Slack Confirm
  hosts: rhautomation
  vars_files:
    - ec2_vars.yaml
  vars:
    response_url: 'https://hooks.slack.com/services/something/something'
  roles:
    - redhatautomation.iac_gitlab_ci.slack
```

```yaml
---
- name: Teardown
  hosts: localhost
  vars_files:
    - ec2_vars.yaml
  vars:
    infra_state: false
  roles:
    - redhatautomation.iac_gitlab_ci.manage_ec2_instances
```

#### Variables
The following variable file is used for defining helpful `ec2` variables:
```yaml
---
ec2_name_prefix: some-prefix
ec2_region: us-east-1
ec2_wait: true
ec2_key: "some-key"
ec2_security_group: "some-sg"
ec2_vpc_subnet_id: "some-subnet"
ec2_count: 3
ssh_port: 22
workshop_type: rhel
rhel: "rhel8"

# additional info needed by AWS ec2 modules
ec2_info:
  rhel8:
    owners: 1234
    size: t2.medium
    os_type: linux
    disk_space: 10
    architecture: x86_64
    filter: 'RHEL-8*HVM-*Hourly*'
    username: ec2-user
  rhel7:
    owners: 123
    size: t2.medium
    os_type: linux
    disk_space: 10
    architecture: x86_64
    filter: 'RHEL-7.7_HVM_GA-20190723-x86_64-1-Access2-GP2'
    username: ec2-user
    python_interpreter: '/usr/bin/python'
```


#### GitLab
The premiss of this demo is its use with a [git](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository) repository. GitLab hosts git repositories and so it will require one hosted either in a GitLab instance or on [GitLab.com](https://gitlab.com).  This collection can be used directly from the GitLab project, see [Installation](#installation).

Within the GitLab project create a `.gitlab-ci.yml` similar to the following:

```yaml
image: quay.io/matferna/awxkit:latest

stages:
  - test
  - deploy
  - sync_inventory
  - configure
  - notify
  - teardown

test:
  stage: test
  image: docker.io/ansible/ansible-runner
  script: |
    ansible-galaxy collection install --api-key $hub_token -s https://hub.redhatgov.io/api/galaxy/content/published/ -r collections/requirements.yml --verbose
    ansible-playbook --syntax-check  *.yml

deploy:
  script: awx job_templates launch --monitor 213 -f human
  only:
    - master
  stage: deploy

sync:
  script: awx inventory_sources update 16 --monitor -f human
  only:
    - master
  stage: sync_inventory

configure:
  only:
    - master
  script: awx job_templates launch --monitor 214 -f human
  stage: configure

notify:
  only:
    - master
  script: awx job_templates launch --monitor 215 -f human
  stage: notify

teardown-manual:
  only:
    - master
  script: awx job_templates launch --monitor 216 -f human
  stage: teardown
  when: manual

teardown-failure:
  only:
    - master
  script: awx job_templates launch --monitor 216 -f human
  stage: teardown
  when: on_failure
```

In order to authenticate with the Tower API, credentials must be stored in GitLab. A simple solution is to create [CI variables](https://docs.gitlab.com/ee/ci/variables/) at the project or group level. 

For the above `.gitlab-ci.yml` we must define two:
  * `TOWER_HOST` with the value of the Ansible Tower URL
  * `TOWER_OAUTH_TOKEN` with the value of a valid [personal access token](ttps://docs.ansible.com/ansible-tower/latest/html/towercli/authentication.html#generating-a-personal-access-token)

Notice that each invocation (`script` sections of) the `gitlab-ci.yml` are calling `awx job_templates launch <ID>`. These IDs are specific to the Ansible Tower server. When job templates are created the ID is visible in the URL. Alternatively it is possible to use [awxkit](https://docs.ansible.com/ansible-tower/latest/html/towercli/reference.html#awx-job-templates-list) or the [Tower API](https://docs.ansible.com/ansible-tower/latest/html/towerapi/browseable.html) to list job templates.

It may also be helpful to configure a [GitLab Runner](https://docs.gitlab.com/runner/).

## NAPS DA ServiceNow Self-Service Demo
These roles are used to build the [IaC with GitLab CI/CD](https://napsda.github.io/ansible/links/infra-as-code2.html).

## References
* https://docs.ansible.com/ansible/latest/collections/amazon/aws/ec2_module.html
* https://docs.gitlab.com/ee/ci/variables/
* https://docs.gitlab.com/ee/ci/

