# redhatautomation.iac_gitlab_ci.slack

This role sends a [Slack](https://slack.com/) message for use in the Infrastructure as Code with GitLab CI demonstration.

## Variables
The variable `response_url` must be defined in order for this role to function. See [Slack's webhook documentation](https://api.slack.com/messaging/webhooks) for more information.

The following is an example of how this variable could be set.
```
response_url: https://hooks.slack.com/services/some/webhook/thing
```

## Refernces
* https://docs.ansible.com/ansible/2.3/uri_module.html
* https://api.slack.com/messaging/webhooks
