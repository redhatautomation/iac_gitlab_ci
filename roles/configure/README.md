# redhatautomation.iac_gitlab_ci.configure

This ansible role deploys a webserver.

## Variables

For RHEL (or most fedora-family distributions) you can define the following variables and use this role:

```yaml
packages:
  - httpd
  - firewalld
  - python3-mod_wsgi

sites_enabled_dir: '/etc/httpd/conf/sites-enabled'
```
