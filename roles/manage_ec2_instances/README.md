# redhatautomation.iac_gitlab_ci.manage_ec2_instances

This role is mainly taken from the [Ansible Workshops](https://github.com/ansible/workshops/tree/devel/provisioner/roles/manage_ec2_instances) project. It is used to provision [ec2](https://aws.amazon.com/ec2/?ec2-whats-new.sort-by=item.additionalFields.postDateTime&ec2-whats-new.sort-order=desc) instances.

